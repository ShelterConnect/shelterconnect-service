package io.jkratz.shelterconnect

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ShelterConnectApplication

fun main(args: Array<String>) {
    runApplication<ShelterConnectApplication>(*args)
}
